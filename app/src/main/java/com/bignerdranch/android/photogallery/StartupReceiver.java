package com.bignerdranch.android.photogallery;

//BroadcastReveiver handles intents.
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by ergerodr on 8/3/2016.
 */
public class StartupReceiver extends BroadcastReceiver {
    private static final String TAG = "StartupReceiver: ";

    @Override//Intents received handled here.
    public void onReceive(Context context, Intent intent){
        Log.i(TAG, "Recieved Broadcast intent: " + intent.getAction());

        boolean isOn = QueryPreferences.isAlarmOn(context);
        PollService.setServiceAlarm(context, isOn);
    }

}
