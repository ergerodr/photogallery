package com.bignerdranch.android.photogallery;

import android.net.Uri;

/**
 * Created by ergerodr on 7/30/2016.
 */
public class GalleryItem {
    private String mCaption;
    private String mCaptionID;
    private String mURL;
    private String mOwner;

    @Override
    public String toString() {
        return mCaption;
    }


    public String getOwner() {
        return mOwner;
    }

    public void setOwner(String mOwner) {
        this.mOwner = mOwner;
    }

    public Uri getPhotoPageUri() {
        return Uri.parse("http://www.flickr.com/photos/")
                .buildUpon()
                .appendPath(mOwner)
                .appendPath(mCaptionID)
                .build();
    }

    public String getCaption() {
        return mCaption;
    }

    public void setCaption(String mCaption) {
        this.mCaption = mCaption;
    }

    public String getID() {
        return mCaptionID;
    }

    public void setID(String mCaptionID) {
        this.mCaptionID = mCaptionID;
    }

    public String getURL() {
        return mURL;
    }

    public void setURL(String mURL) {
        this.mURL = mURL;
    }

}
