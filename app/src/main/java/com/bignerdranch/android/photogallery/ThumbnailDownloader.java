package com.bignerdranch.android.photogallery;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by ergerodr on 7/30/2016.
 *
 * MESSAGE LOOP:  A thread that works by using a message queue. Consists of a
 *      thread and a looper.
 *
 *  LOOPER: Object that manages a thread's message queue. (represented by arrows in FIG: 24.3)
 *      ThumbnailDownloader below is our Looper.
 *
 *  MESSAGE: Contains a WHAT, OBJ, and TARGET.
 *      TARGET: the Handler that will handle the message.
 *      OBJ: a user-specified object to be sent with the message.
 *      WHAT: a user-defined int that describes the message.
 *
 *  HANDLER: interface for creating and posting Messages. Also processes messages. because
 *      messages must be posted and consumed on a looper, the Handler always has a reference to
 *      the looper.
 *
 *      "mRequestHandler in this Case"
 *
 */
public class ThumbnailDownloader<T> extends HandlerThread {
    private static final String TAG = "ThumbnailDownloader";
    private static final int MESSAGE_DOWNLOAD = 0;

    private Handler mRequestHandler;
    //responseHandler is a handler attached to the main thread
    private Handler mResponseHandler;
    private ThumbnailDownloadListener<T> mThumbnailDownloadListener;
    //Thread-safe version of HashMap.
    private ConcurrentMap<T, String> mRequestMap = new ConcurrentHashMap<>();

    //Used to communicate the responses(dwnloaded IMGs), to the UI. This delegates the
    //Responsibility of what to do with the downloaded image to PhotoGalleryFragment.
    public interface ThumbnailDownloadListener<T> {
        void onThumbnailDownloaded(T target, Bitmap Thumbnail);
    }

    public void setThumbnailDownloadListener(ThumbnailDownloadListener<T> listener){
        mThumbnailDownloadListener = listener;
    }

    public ThumbnailDownloader(Handler responseHandler) {
        super(TAG);
        mResponseHandler = responseHandler;
    }


    //Called before the Looper checks the queue for the first time. 
    @Override
    protected void onLooperPrepared(){
        mRequestHandler = new Handler(){
            @Override
            public void handleMessage(Message msg){
                if(msg.what == MESSAGE_DOWNLOAD){
                    T target = (T) msg.obj;
                    Log.i(TAG, "Got a request for URL: " + mRequestMap.get(target));
                    handleRequest(target);
                }
            }
        };
    }

    //Download bytes from the URL and then turn these bytes into a bitmap.
    //  See "FlickrFetcher" class
    private void handleRequest(final T target){
        try{
            final String url = mRequestMap.get(target);

            if(url == null) {
                return;
            }
            byte[] bitmapBytes = new FlickrFetcher().getUrlBytes(url);
            final Bitmap bitmap = BitmapFactory
                    .decodeByteArray(bitmapBytes, 0, bitmapBytes.length);
            Log.i(TAG, "Bitmap created");

            //How is mResponseHandler associated with the main Thread's Looper?
            //    A: See Constructor for thumbnailDownloader and where thumbnailDownloader is
            //instantiated.
            mResponseHandler.post(new Runnable() {
                //Need a way to post message to main thread, requesting to add image to UI.
                //This is what "run()" does.
                public void run() {
                    //RecyclerView may have already requested a new URL. check queue.
                    if(mRequestMap.get(target) != url){
                        return;
                    }
                    mRequestMap.remove(target);
                    mThumbnailDownloadListener.onThumbnailDownloaded(target, bitmap);
                }
            });
        } catch(IOException ioe){
            Log.e(TAG, "Error downloading image", ioe);
        }
    }

    //Obtains and sends messages
    public void queueThumbnail(T target, String url){
        Log.i(TAG, "Got a URL: " + url);

        if(url == null) {
            mRequestMap.remove(target);
        } else {
            //Handler puts the message on the queue, along with a photoHolder.
            mRequestMap.put(target, url);
            //Builds the message here by providing "obtainMessage(WHAT, OBJ)". the url is not
            //   put in the message because it is tied to the target upon Hashmap lookup.
            mRequestHandler.obtainMessage(MESSAGE_DOWNLOAD, target)
                    .sendToTarget();
            //sendToTarget calls Handler.HandleMessage() and assigns to
            // the Handler "mRequestHandler". (I.E., the TARGET)
        }
    }

    //Remove any pending posts of messages with code 'what' that are in the message queue.
    public void clearQueue(){
        mRequestHandler.removeMessages(MESSAGE_DOWNLOAD);
    }
}
