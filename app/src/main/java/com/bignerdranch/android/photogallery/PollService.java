package com.bignerdranch.android.photogallery;

//System Service that can send intents for you. (PendingIntent)
import android.app.Activity;
import android.app.AlarmManager;

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import java.util.List;

/**
 * Created by ergerodr on 8/1/2016.
 */
public class PollService extends IntentService {
    private final static String TAG = "PollService";
    private static final int POLL_INTERVAL = 1000 * 20; //60 Seconds
    public static final String ACTION_SHOW_NOTIFICATION =
            "com.bignerdranch.android.photogallery.SHOW_NOTIFICATION";
    public static final String PERM_PRIVATE =
            "com.bignerdranch.android.photogallery.PRIVATE";
    public static final String REQUEST_CODE = "REQUEST_CODE";
    public static final String NOTIFICATION = "NOTIFICATION";

    public static Intent newIntent(Context context){
        return new Intent(context, PollService.class);
    }
    //      "getService()" Parameters
    //1) Context with which to send intent
    //2) Request code to distinguish between PendingIntents
    //3) The intent object to send
    //4) Flags distinguishing how the PendingIntent is created.
    public static void setServiceAlarm(Context context, boolean isOn){
        //  If you request a PendingIntent twice with the same intent,
        //you will get the same PendingIntent.
        //  You can use this to test whether a PendingIntent already exists
        //or to cancel a previously issuedPendingIntent.
        Intent i = PollService.newIntent(context);
        PendingIntent pi = PendingIntent.getService(context, 0, i, 0);

        AlarmManager alarmManager = (AlarmManager) context
                .getSystemService(context.ALARM_SERVICE);
        if(isOn){//Set the Alarm Here
            alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime(), POLL_INTERVAL, pi);
        } else {//Cancel the Alarm and PendingIntent
            alarmManager.cancel(pi);
            pi.cancel();
        }
        //save alarm state
        QueryPreferences.setAlarmOn(context, isOn);
    }

    //We can only register one alarm for each unique PendingIntent
    public static boolean isServiceAlarmOn(Context context){
        Intent i = PollService.newIntent(context);
        PendingIntent pi = PendingIntent
                .getService(context, 0, i, PendingIntent.FLAG_NO_CREATE);
        //Flag above returns null if the PendingIntent does not already exist instead of creating the service.
        return pi != null;
    }//null "pi" here means that the alarm is not set.

    public PollService(){
        super(TAG);
    }
/*  IntentService fires up a background thread and
 *      puts the commands it receives on a FIFO queue.
 *
 *  When there are no commands in the queue the service is destroyed.
 *
 *  onHandleIntent(intent): Called for each command in the queue.
 */
    @Override
    protected void onHandleIntent(Intent intent){
        if(!isNetworkAvailableAndConnected()){
            return;
        }
        //Pull out the current and lastResultID from shared Preferences.
        String query = QueryPreferences.getStoredQuery(this);
        String lastResultId = QueryPreferences.getLastResultId(this);
        List<GalleryItem> items;
        //Fetch Results from flickr
        if(query == null){
            items = new FlickrFetcher().fetchRecentPhotos();
        } else {
            items = new FlickrFetcher().searchPhotos(query);
        }
        if (items.size() == 0){
            return;//no results found
        }
        //Grab the first result
        String resultId = items.get(0).getID();
        if (resultId.equals(lastResultId)){
            Log.i(TAG, "Got an old Result " + resultId);
        } else {
            Log.i(TAG, "Got a new result " + resultId);

            Resources resources = getResources();
            Intent i = PhotoGalleryActivity.newIntent(this);
            PendingIntent pi = PendingIntent.getActivity(this, 0, i, 0);

            Notification notification = new NotificationCompat.Builder(this)
                    .setTicker(resources.getString(R.string.new_pictures_title))
                    .setSmallIcon(android.R.drawable.ic_menu_report_image)
                    .setContentTitle(resources.getString(R.string.new_pictures_title))
                    .setContentText(resources.getString(R.string.new_pictures_text))
                    .setContentIntent(pi)
                    .setAutoCancel(true)
                    .build();

            showBackgroundNotification(0, notification);
        }
        QueryPreferences.setLastResultId(this, resultId);
    }

    private void showBackgroundNotification(int requestCode, Notification notification){
        Intent i = new Intent(ACTION_SHOW_NOTIFICATION);
        i.putExtra(REQUEST_CODE,requestCode);
        i.putExtra(NOTIFICATION, notification);
        sendOrderedBroadcast(i, PERM_PRIVATE, null, null,
                Activity.RESULT_OK, null, null);
    }

    private boolean isNetworkAvailableAndConnected(){
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

        boolean isNetworkAvailable = cm.getActiveNetworkInfo() != null;
        boolean isNetworkConnected = isNetworkAvailable &&
                cm.getActiveNetworkInfo().isConnected();

        return isNetworkConnected;
    }
}
